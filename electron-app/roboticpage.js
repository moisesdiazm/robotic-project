const REST_API_URL = "http://localhost:8080"
//INITIALIZE VALUES
var lastSelected = '';

document.addEventListener('DOMContentLoaded', function() {
  document.getElementById('badge1').innerHTML = (inputElement1.value*180/1023).toFixed(2) + "º";
  document.getElementById('badge2').innerHTML = (inputElement1.value*180/1023).toFixed(2) + "º";
  document.getElementById('xvalue').innerHTML = 0;
  document.getElementById('yvalue').innerHTML = 0;
  document.getElementById('calculatedAngle1').innerHTML = 0;
  document.getElementById('calculatedAngle2').innerHTML = 0;

  var offsetX = 15, offsetY = 25;
  let cx = document.querySelector("canvas").getContext("2d");
    
  //canvas generation
  cx.strokeStyle = "gray";
  cx.strokeRect(offsetX, offsetY, 300, 300);
  tabcontent = document.getElementsByClassName("tabcontent");

  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
}, false);


// You can also require other files to run in this process
require('./renderer.js');

function openCity(evt, cityName) {
// Declare all variables
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  tablinks = document.getElementsByClassName("tablinks");

  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}


// LISTENER FOR PAINTING BUTTON

buttonPaint = document.getElementById('switch-1');
buttonPaint.addEventListener('change', function(){

  var retVal = 0;
  if(this.checked){
    retVal = 1;
  }
  else {
    retVal = 0;
  }

  $.ajax({
    url: REST_API_URL + "/updateServo",
    type: "get",
    data: {servo: 3, value: retVal},
    success: function(response) {
      console.log(retVal);
    },
    error: function(xhr) {
      //Do Something to handle error
      console.log("error");
    }
  });
})

// LISTENER FOR SLIDER 1

inputElement1 = document.getElementById("slider1");
inputElement1.addEventListener('input', function(evt){
  console.log("Event");
  // document.getElementById('badge1').innerHTML = (inputElement1.value*180/868).toFixed(2) + "º";
  document.getElementById('badge1').innerHTML = (inputElement1.value*1.0).toFixed(1) + "º";
  $.ajax({
      url: REST_API_URL + "/updateServo",
      type: "get",
      data: {servo: 1, value: inputElement1.value},
      success: function(response) {
        console.log(inputElement1.value);
      },
      error: function(xhr) {
        //Do Something to handle error
        console.log("error");
      }
    });
});

// LISTENER FOR SLIDER 2

inputElement2 = document.getElementById("slider2");
inputElement2.addEventListener('input', function(evt){
  console.log("Event");
  // document.getElementById('badge2').innerHTML = (inputElement2.value*151.17/769).toFixed(2) + "º";
  document.getElementById('badge2').innerHTML = (inputElement2.value*1.0).toFixed(1) + "º";
  $.ajax({
      url: REST_API_URL + "/updateServo",
      type: "get",
      data: {servo: 2, value: inputElement2.value},
      success: function(response) {
        console.log(inputElement2.value);
      },
      error: function(xhr) {
        //Do Something to handle error
        console.log("error");
      }
 
  });
});

//DIRECT KINEMATICS SEND BUTTON
function sendDirectKinematic(){
  console.log("event: send Kinematic");

  angle1 = document.getElementById("angle1").value;
  angle2 = document.getElementById("angle2").value;
  console.log(`Angle 1 {angle1}, Angle 2 {angle2} -> Pos X {response.posx}, Pos Y {response.posy}`);
  $.ajax({
      url: REST_API_URL + "/directKinematics",
      type: "get",
      // data: {articulation1: angle1, articulation2: angle2},
      data: {articulation1: angle1, articulation2: angle2},
      success: function(response) {
        console.log("Angle 1 {angle1}, Angle 2 {angle2} -> Pos X " +response.posx + ", Pos Y " + response.posy);
        document.getElementById('xvalue').innerHTML = response.posx.toFixed(1)
        document.getElementById('yvalue').innerHTML = response.posy.toFixed(1)
      },
      error: function(xhr) {
        //Do Something to handle error
        console.log("error");
      }
  });
}

//INVERSE KINEMATICS SEND BUTTON
function sendInverseKinematic(){
    console.log("event: send inverse Kinematic");
  
    xcoordinate = document.getElementById("xcoordinate").value;
    ycoordinate = document.getElementById("ycoordinate").value;
    $.ajax({
        url: REST_API_URL + "/inverseKinematics",
        type: "get",
        data: {xcoordinate: xcoordinate, ycoordinate: ycoordinate},
        success: function(response) {
          console.log(response)
          if(!response.message){
            console.log("Angle 1 " +response.angle1 + ", Angle 2 " + response.angle2);
            document.getElementById('calculatedAngle1').innerHTML = response.angle1.toFixed(1)
            document.getElementById('calculatedAngle2').innerHTML = response.angle2.toFixed(1)
            document.getElementById('error').innerHTML = ""
          }else{
            document.getElementById('error').innerHTML = "Invalid position"
          }

        },
        error: function(xhr) {
          //Do Something to handle error
          console.log("error");
        }
    });



}

function getX(cmX){
  px = cmX*20 + 15;
  return px;
}

function getY(cmY){
  py = 300 - 20*cmY + 25;
  return py;
}

function clear(){
    var canvas = document.getElementById('canvas');

    // after doing some rendering
    canvas.width = canvas.width;  // clear the whole canvas
    var offsetX = 15, offsetY = 25;
    let cx = document.querySelector("canvas").getContext("2d");
      
    //canvas generation
    cx.strokeStyle = "gray";
    cx.strokeRect(offsetX, offsetY, 300, 300);
    tabcontent = document.getElementsByClassName("tabcontent");
  
}

function drawRectangle(){
  clear();
  lastSelected = 'rectangle';
  let cx = document.querySelector("canvas").getContext("2d");
  var squareX1 = getX(parseFloat(document.getElementById("squareX1").value));
  var squareY1 = getY(parseFloat(document.getElementById("squareY1").value));
  var squareX2 = getX(parseFloat(document.getElementById("squareX2").value));
  var squareY2 = getY(parseFloat(document.getElementById("squareY2").value));
  var squareX3 = getX(parseFloat(document.getElementById("squareX3").value));
  var squareY3 = getY(parseFloat(document.getElementById("squareY3").value));
  var squareX4 = getX(parseFloat(document.getElementById("squareX4").value));
  var squareY4 = getY(parseFloat(document.getElementById("squareY4").value));

  var squareX1 = getX(parseFloat(document.getElementById("squareX1").value));
  var squareY1 = getY(parseFloat(document.getElementById("squareY1").value));
  var squareX2 = getX(parseFloat(document.getElementById("squareX2").value));
  var squareY2 = getY(parseFloat(document.getElementById("squareY2").value));
  var squareX3 = getX(parseFloat(document.getElementById("squareX3").value));
  var squareY3 = getY(parseFloat(document.getElementById("squareY3").value));
  var squareX4 = getX(parseFloat(document.getElementById("squareX4").value));
  var squareY4 = getY(parseFloat(document.getElementById("squareY4").value));
  //square
  cx.beginPath();
  cx.lineTo(squareX1, squareY1);
  cx.lineTo(squareX2, squareY2);
  cx.lineTo(squareX3, squareY3);
  cx.lineTo(squareX4, squareY4);
  cx.lineTo(squareX1, squareY1);
  cx.strokeStyle = "green";
  cx.stroke();
}

function drawSemicircle(){
  lastSelected = 'semicircle';
  clear();
  let cx = document.querySelector("canvas").getContext("2d");
  var angle = parseFloat(document.getElementById("semicircleAngle").value);
  var semicircleX = getX(parseFloat(document.getElementById("semicircleX").value));
  var semicircleY = getY(parseFloat(document.getElementById("semicircleY").value));
  var semicircleRadius = 20*parseFloat(document.getElementById("semicircleRadius").value);

  cx.beginPath(); 
  cx.arc(semicircleX, semicircleY, semicircleRadius, (360 - angle) * Math.PI/180, 0 * Math.PI/180);
  cx.strokeStyle = "red";
  cx.stroke();
}

function drawLine(){
  lastSelected = 'line';
  clear();
  let cx = document.querySelector("canvas").getContext("2d");
  var lineX1 = getX(parseFloat(document.getElementById("lineX1").value));
  var lineY1 = getY(parseFloat(document.getElementById("lineY1").value));
  var lineX2 = getX(parseFloat(document.getElementById("lineX2").value));
  var lineY2 = getY(parseFloat(document.getElementById("lineY2").value));

  // rect line
  cx.beginPath();
  cx.lineTo(lineX1, lineY1);
  cx.lineTo(lineX2, lineY2);
  cx.strokeStyle = "orange";
  cx.stroke();
  
}

function drawTriangle(){
  lastSelected = 'triangle'
  clear();
  let cx = document.querySelector("canvas").getContext("2d");

  var triangleX1 = getX(parseFloat(document.getElementById("triangleX1").value));
  var triangleY1 = getY(parseFloat(document.getElementById("triangleY1").value));
  var triangleX2 = getX(parseFloat(document.getElementById("triangleX2").value));
  var triangleY2 = getY(parseFloat(document.getElementById("triangleY2").value));
  var triangleX3 = getX(parseFloat(document.getElementById("triangleX3").value));
  var triangleY3 = getY(parseFloat(document.getElementById("triangleY3").value));


  //triangle
  cx.beginPath();
  cx.lineTo(triangleX1, triangleY1);
  cx.lineTo(triangleX2, triangleY2);
  cx.lineTo(triangleX3, triangleY3);
  cx.lineTo(triangleX1, triangleY1);
  cx.strokeStyle = "blue";
  cx.stroke();
    
}

function drawCircle(){
  lastSelected = 'circle'
  clear();
  let cx = document.querySelector("canvas").getContext("2d");
  var circleX = getX(parseFloat(document.getElementById("circleX").value));
  var circleY = getY(parseFloat(document.getElementById("circleY").value));
  var circleRadius = 20*parseFloat(document.getElementById("circleRadius").value);
  
  // circle     
  cx.beginPath(); 
  cx.arc(circleX, circleY, circleRadius, 0, 2 * Math.PI);
  cx.strokeStyle = "black";
  cx.stroke();

}

//DIRECT KINEMATICS SEND BUTTON
function sendFigure(){
  console.log("event: send Figure");

  switch(lastSelected){
    case 'triangle':
      var triangleX1 = parseFloat(document.getElementById("triangleX1").value);
      var triangleY1 = parseFloat(document.getElementById("triangleY1").value);
      var triangleX2 = parseFloat(document.getElementById("triangleX2").value);
      var triangleY2 = parseFloat(document.getElementById("triangleY2").value);
      var triangleX3 = parseFloat(document.getElementById("triangleX3").value);
      var triangleY3 = parseFloat(document.getElementById("triangleY3").value);

      body = {
        type: 'triangle',
        datapoints: [
          [triangleX1, triangleY1],
          [triangleX2, triangleY2],
          [triangleX3, triangleY3]
        ]
      }
    break;
    case 'rectangle':
      var squareX1 = parseFloat(document.getElementById("squareX1").value);
      var squareY1 = parseFloat(document.getElementById("squareY1").value);
      var squareX2 = parseFloat(document.getElementById("squareX2").value);
      var squareY2 = parseFloat(document.getElementById("squareY2").value);
      var squareX3 = parseFloat(document.getElementById("squareX3").value);
      var squareY3 = parseFloat(document.getElementById("squareY3").value);
      var squareX4 = parseFloat(document.getElementById("squareX4").value);
      var squareY4 = parseFloat(document.getElementById("squareY4").value);

      body = {

        type: 'rectangle',
        datapoints: [
          [squareX1, squareY1],
          [squareX2, squareY2],
          [squareX3, squareY3],
          [squareX4, squareY4]
        ]
      }
    break;
    case 'line':

      var lineX1 = parseFloat(document.getElementById("lineX1").value);
      var lineY1 = parseFloat(document.getElementById("lineY1").value);
      var lineX2 = parseFloat(document.getElementById("lineX2").value);
      var lineY2 = parseFloat(document.getElementById("lineY2").value);

      body = {

        type: 'line',
        datapoints: [
          [lineX1, lineY1],
          [lineX2, lineY2]
        ]
      }
    break;
    case 'circle':
      
      var circleX = parseFloat(document.getElementById("circleX").value);
      var circleY = parseFloat(document.getElementById("circleY").value);
      var circleRadius = parseFloat(document.getElementById("circleRadius").value);

      body = {
        type: 'circle',
        center: [circleX, circleY],
        radius: circleRadius
      }
    break;
    case 'semicircle':
      var angle = parseFloat(document.getElementById("semicircleAngle").value);
      var semicircleX = parseFloat(document.getElementById("semicircleX").value);
      var semicircleY = parseFloat(document.getElementById("semicircleY").value);
      var semicircleRadius = parseFloat(document.getElementById("semicircleRadius").value);
      body = {
        type: 'semicircle',
        center: [semicircleX, semicircleY],
        radius: semicircleRadius,
        angle: angle
      }
    break;
  }

  console.log('Sending : ', lastSelected);
  $.ajax({
      url: REST_API_URL + "/drawfigure",
      data: JSON.stringify(body),
      dataType: 'json',
      type: 'post',
      contentType: 'application/json',
      success: function(response) {
        console.log("ACCEPTED");
        // document.getElementById('xvalue').innerHTML = response.posx.toFixed(1)
        // document.getElementById('yvalue').innerHTML = response.posy.toFixed(1)
      },
      error: function(xhr) {
        //Do Something to handle error
        console.log("error");
      }
  });
}