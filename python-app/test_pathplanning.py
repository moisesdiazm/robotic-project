from robotic.path_planning import PathPlanner
import matplotlib.pyplot as plt

pp = PathPlanner()

trayectories = pp.getTrayectory([(0,0),(14,7)])


line1 = trayectories[0]
x1, y1 = zip(*line1)
line2 = trayectories[1]
x2, y2 = zip(*line2)


plt.scatter(x1,y1)
plt.scatter(x2,y2)

plt.show()