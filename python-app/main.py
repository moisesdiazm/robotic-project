from flask import Flask, jsonify, request
from flask_restful import reqparse
import serial, time
from time import sleep

from robotic.robotic_model import Robot
from robotic.path_planning import PathPlanner


DEBUG = True

if not DEBUG:
	arduino = serial.Serial('/dev/tty.usbmodem641', 9600)

#Initialize robotic model
# robot = Robot(dx=7.5, dy=8.5, r0=0, rB=0, r1=0, L1=14, L2=11, dxx=-1.7, dyy=2.1)
robot = Robot(dx=7.5, dy=8.5, r0=0, rB=0, r1=0, L1=14, L2=11.26, actuatorAngle=15)
pp = PathPlanner(presicion=0.01)
#Initialize app
app = Flask(__name__)
parser = reqparse.RequestParser()
parser.add_argument('servo')
parser.add_argument('value')
parser.add_argument('articulation1')
parser.add_argument('articulation2')
parser.add_argument('xcoordinate')
parser.add_argument('ycoordinate')

last_angle = [0,0]


def mapvalue(value, leftMin, leftMax, rightMin, rightMax):
    # Figure out how 'wide' each range is
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin

    # Convert the left range into a 0-1 range (float)
    valueScaled = float(value - leftMin) / float(leftSpan)

    # Convert the 0-1 range into a value in the right range.
    return rightMin + (valueScaled * rightSpan)

def servoWrite(servoNum, servoValue):

	# Added for more presicion model
	if servoNum == 2:
		servoValue = mapvalue(servoValue, 0, 90, 1960, 1200)
	elif servoNum == 1:
		servoValue = mapvalue(servoValue, 0, 180, 500, 2100)


	payload = "." + str(servoNum) + "{0:04d}".format(int(servoValue)) + ","
	if not DEBUG:
		arduino.write(payload.encode('ascii'))
	# else:
	# 	print(payload)

	# Save last servo position for inverse kinematics matters
	if servoNum == 1:
		last_angle[0] = servoValue
	elif servoNum == 2:
		#last_angle[1] = servoValue
		last_angle[1] = servoValue

	return payload


def printDataPoints(trayectory):
	for i in range(len(trayectory)):

		# Make calculus of direct kinematics
		try:
			# print(datapoint)
			angle1, angle2 = robot.inverseKinematics(*trayectory[i], *last_angle)
			# print(angle1, angle2)
			# Send data to servos 
			# servoWrite(1, int(angle1*868/180.00)) 
			# servoWrite(2, int(angle2*769/151.17))
			servoWrite(1, int(angle1)) 
			servoWrite(2, int(angle2))
			sleep(0.001)
		except Exception:
			raise Exception("Invalid position")
		
		if i == 0:
			sleep(0.5)
			# START DRAWING expand actuator
			servoWrite(3, 1)

@app.route('/updateServo')
def updateServo():
    body = parser.parse_args()
	# servoNum = int(body['servo'])
    # value = int(body['value'])
    servoNum = int(body['servo'])
    value = float(body['value'])
    print(servoNum, "; ", value)
    servoWrite(servoNum, value)
    return "Success"


@app.route('/directKinematics')
def directKinematics():

	body = parser.parse_args()
	# angle1 = int(body['articulation1'])
	# angle2 = int(body['articulation2'])
	angle1 = float(body['articulation1'])
	angle2 = float(body['articulation2'])

	# Make calculus of direct kinematics
	# posX, posY = robot.directKinematics(int(angle1), int(angle2)) #Calculus with degree angles
	posX, posY = robot.directKinematics(int(angle1), int(angle2)) #Calculus with degree angles

	# Send data to servos 
	# Using high presicion representation for angles
	# servoWrite(1, int(angle1*868/180.00)) 
	# servoWrite(2, int(angle2*769/151.17))
	servoWrite(1, int(angle1)) 
	servoWrite(2, int(angle2))
	# Return position
	return jsonify({'posx': posX, 'posy': posY})

@app.route('/inverseKinematics')
def inverseKinematics():

	body = parser.parse_args()
	# xcoordinate = int(body['xcoordinate'])
	# ycoordinate = int(body['ycoordinate'])
	xcoordinate = float(body['xcoordinate'])
	ycoordinate = float(body['ycoordinate'])
	# Make calculus of direct kinematics
	try:
		angle1, angle2 = robot.inverseKinematics(xcoordinate, ycoordinate, *last_angle)
	except Exception:
		return jsonify({'message': 'Invalid position reached'})

	# Send data to servos 
	# servoWrite(1, int(angle1*868/180.00)) 
	# servoWrite(2, int(angle2*769/151.17))
	servoWrite(1, angle1) 
	servoWrite(2, angle2)
	# Return position
	return jsonify({'angle1': angle1, 'angle2': angle2})

@app.route('/drawfigure', methods=['POST'])
def drawfigure():
	body = request.get_json()



	if(body['type'] in ['triangle', 'rectangle', 'line']):
		coords = list(map(lambda x: tuple(x), body['datapoints']))
		trayectories = pp.getTrayectory(coords, closed=False) if body['type'] == 'line' else pp.getTrayectory(coords, closed=True) 
		for i in range(len(trayectories)):
			print("DRAWING LINE ", i)
			try:
				printDataPoints(trayectories[i])
			except:
				return jsonify({'message': 'Invalid position reached'}),500
	
	elif(body['type'] == 'circle'):
		print("CIRCLE")
		trayectory = pp.getCurves(*body['center'], body['radius'])
		try:
			printDataPoints(trayectory)
		except:
			return jsonify({'message': 'Invalid position reached'}),500
	
	# FINISH DRAWING retrieve actuator
	sleep(2)
	servoWrite(3, 0)

	print("FINISHED DRAWING")
	return jsonify({'message': 'Successfully completed'}), 200

if __name__ == "__main__":
    app.run(port=8080, debug=True)
    arduino.close()