import numpy as np

class Robot(object):
	""" Class that represents the robotic environment for the 2 DOF robot"""

	def __init__(self, dx, dy, r0, rB, r1, L1, L2, actuatorAngle):
		self.dx =  dx
		self.dy = dy
		self.r0 = r0
		self.rB = rB
		self.r1 = r1
		self.L1 = L1
		self.L2 = L2
		self.actuatorAngle = actuatorAngle
		self.T0B, self.TB0 = self.generateTransformationMatrixes()


	def generateTransformationMatrixes(self):
		rotationAngle = np.deg2rad(90) + np.deg2rad(self.rB) - np.deg2rad(self.r0)
		rotationMatrixOB = np.array([
			[np.cos(rotationAngle), - np.sin(rotationAngle)],
			[np.sin(rotationAngle), np.cos(rotationAngle)]
		])
		translationVector = np.array([[self.dx, self.dy]])
		T0B = np.concatenate((rotationMatrixOB,translationVector.T), axis=1) # Append translation to rotation
		T0B = np.concatenate((T0B, np.array([[0,0,1]])), axis=0) # Normalize matrix

		# Calculating TB0

		rotationT = rotationMatrixOB.T
		translation = - np.matmul(rotationT,translationVector.T)

		TB0 = np.concatenate((rotationT, translation), axis=1)
		TB0 = np.concatenate((TB0, np.array([[0,0,1]])), axis=0) # Normalize matrix

		# print("TB0", TB0)
		# print("T0B", T0B)
		return T0B, TB0

	def directKinematics(self, angle1, angle2):

		p0x = self.L1 * np.cos(np.deg2rad(angle1)) + self.L2 * np.cos(np.deg2rad(angle1) + np.deg2rad(angle2) + np.deg2rad(self.actuatorAngle))
		p0y = self.L1 * np.sin(np.deg2rad(angle1)) + self.L2 * np.sin(np.deg2rad(angle1) + np.deg2rad(angle2) + np.deg2rad(self.actuatorAngle))

		pB = np.matmul(self.TB0, np.array([p0x,p0y,1]))

		# p2 = np.matmul(self.T23, pB)
		# print("punto XY {}".format(np.array([p0x,p0y,1])))
		# # print("punto P2 {}".format(p2))
		# print("punto pB {}".format(pB))
		return np.around(pB[0],4), np.around(pB[1],4) #return x and y coordinates with respect to frame B
	
	def inverseKinematics(self, posX, posY, lastTheta1, lastTheta2):
		
		p0 = np.matmul(self.T0B, [posX, posY, 1])
		p0x, p0y = p0[0], p0[1]

		alphaArg = (p0x**2 + p0y**2 + self.L1**2 - self.L2**2)/(2*self.L1*np.sqrt(p0x**2 + p0y**2))
		betaArg = (self.L1**2 + self.L2**2 - p0x**2 - p0y**2)/(2*self.L1*self.L2)
	
		if (alphaArg >= -1 and alphaArg <= 1) and (betaArg >= -1 and betaArg <= 1):
			# valid range
			alpha = np.arccos(alphaArg)
			beta = np.arccos(betaArg)
		else:
			raise Exception("Invalid Angles")

		# Solution comparison
		# Solution for elbow down
		theta2down = 180 - np.rad2deg(beta)
		theta1down = np.rad2deg(np.arctan2(p0y,p0x)) - np.rad2deg(alpha)

		deltaDown = abs(theta2down - lastTheta2) + abs(theta1down - lastTheta1)

		# # Solution for elbow up
		theta2up = np.rad2deg(beta) - 180
		theta1up = np.rad2deg(np.arctan2(p0y,p0x)) + np.rad2deg(alpha)
	
		deltaUp = abs(theta2up - lastTheta2) + abs(theta1up - lastTheta1)

		return (theta1up, theta2up - self.actuatorAngle) if deltaDown > deltaUp else (theta1down, theta2down - self.actuatorAngle)