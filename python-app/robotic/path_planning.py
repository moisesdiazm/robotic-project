import numpy as np
import matplotlib.pyplot as plt

class PathPlanner(object):

    def __init__(self, presicion=0.1):
        self.presicion = presicion #cm
    

    def getRange(self, val1, val2):
        if (val2 > val1):
            ordered_range = np.arange(val1,val2,self.presicion)
        else:
            ordered_range = np.arange(val2,val1,self.presicion)
            ordered_range = list(reversed(ordered_range))

        return ordered_range
    
    def buildLine(self,x1,y1,x2,y2):
        x_samples = self.getRange(x1,x2)

        if abs(x2-x1) > 0:
            y_samples = list(map(lambda x: (y2-y1)/(x2-x1)*(x-x1) + y1, x_samples))
        else: # vertical line
            y_samples = self.getRange(y1,y2) # Generation of y values because it is a vertical line (not a function)
            x_samples = np.zeros(len(y_samples)) + x1 # generation of x values repeated because there is not range in x (same value all the time for this case)

        coordinate_tuples = [(x_samples[i],y_samples[i]) for i in range(len(x_samples))]

        return coordinate_tuples

    
    def getCurves(self, h, k, r):
        i = 0
        j = 0
        x = h - r
        y = k

        xy_pair = []
        
        x = (h-r) + i*(self.presicion)
        y = (2*k + np.sqrt(-4*((x-h)**2) + 4*(r**2)))/2
        xy_pair.append((x,y))
        while(x < (h+r)):
            i += 1
            x = (h-r) + i*(self.presicion)
            y = (2*k + np.sqrt(-4*((x-h)**2) + 4*(r**2)))/2
            xy_pair.append((x,y))

        x = (h+r) - j*(self.presicion)
        y = (2*k + np.sqrt(-4*((x-h)**2) + 4*(r**2)))/2
        xy_pair.append((x,y))
        while(x > (h-r)):
            j += 1
            x = (h+r) - j*(self.presicion)
            y = (2*k - np.sqrt(-4*((x-h)**2) + 4*(r**2)))/2
            xy_pair.append((x,y))
        
        x,y = zip(*xy_pair)
        plt.scatter(x,y)
        plt.savefig('circle.png')
        print(xy_pair)
        return xy_pair

    def getTrayectory(self, coords, closed=True): #tuple of coordinates
        lines = []
        for i in range(len(coords)):
            if i == (len(coords) - 1):
                if closed:
                    lineTrayectory = self.buildLine(*coords[i], *coords[0])
                else:
                    continue
            else:
                lineTrayectory = self.buildLine(*coords[i], *coords[i+1])
            lines.append(lineTrayectory)

        for line in lines:
            x, y = zip(*line)
            plt.scatter(x,y)
            plt.savefig('circle.png')
        return lines #return a list of lines, each line is list of pair coordinates
        
